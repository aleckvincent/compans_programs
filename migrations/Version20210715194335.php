<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210715194335 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE outgoing_speech (id INT AUTO_INCREMENT NOT NULL, weekend_id INT DEFAULT NULL, INDEX IDX_C7DBACFEA32EAF0F (weekend_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE outgoing_speech ADD CONSTRAINT FK_C7DBACFEA32EAF0F FOREIGN KEY (weekend_id) REFERENCES weekend (id)');
        $this->addSql('ALTER TABLE weekend ADD type VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE weekend_speaker ADD is_outgoing TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE outgoing_speech');
        $this->addSql('ALTER TABLE weekend DROP type');
        $this->addSql('ALTER TABLE weekend_speaker DROP is_outgoing');
    }
}
