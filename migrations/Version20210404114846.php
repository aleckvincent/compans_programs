<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210404114846 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chairman (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, UNIQUE INDEX UNIQ_834095AD217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(128) NOT NULL, last_name VARCHAR(128) NOT NULL, email VARCHAR(255) DEFAULT NULL, cell_phone VARCHAR(16) DEFAULT NULL, home_phone VARCHAR(16) DEFAULT NULL, address_line_1 VARCHAR(255) DEFAULT NULL, address_line_2 VARCHAR(255) DEFAULT NULL, address_line_3 VARCHAR(255) DEFAULT NULL, birthday DATE DEFAULT NULL, baptism_day DATE DEFAULT NULL, is_baptized TINYINT(1) DEFAULT NULL, is_elder TINYINT(1) DEFAULT NULL, is_servant TINYINT(1) DEFAULT NULL, gender VARCHAR(2) NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speech (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, number INT DEFAULT NULL, category VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, forgot_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend (id INT AUTO_INCREMENT NOT NULL, program_id INT NOT NULL, speaker_id INT DEFAULT NULL, reader_id INT DEFAULT NULL, chairman_id INT DEFAULT NULL, speech_id INT DEFAULT NULL, date DATETIME NOT NULL, is_external TINYINT(1) DEFAULT NULL, external_name VARCHAR(255) DEFAULT NULL, is_overseer TINYINT(1) DEFAULT NULL, overseer_name VARCHAR(255) DEFAULT NULL, is_convention TINYINT(1) DEFAULT NULL, INDEX IDX_AED2CDAE3EB8070A (program_id), INDEX IDX_AED2CDAED04A0F27 (speaker_id), INDEX IDX_AED2CDAE1717D737 (reader_id), INDEX IDX_AED2CDAECD0B344F (chairman_id), INDEX IDX_AED2CDAEBBC049D6 (speech_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend_program (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, status INT NOT NULL, INDEX IDX_644901DFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend_reader (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, UNIQUE INDEX UNIQ_D16F89E9217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend_speaker (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_8D21AD3A217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chairman ADD CONSTRAINT FK_834095AD217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAE3EB8070A FOREIGN KEY (program_id) REFERENCES weekend_program (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAED04A0F27 FOREIGN KEY (speaker_id) REFERENCES weekend_speaker (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAE1717D737 FOREIGN KEY (reader_id) REFERENCES weekend_reader (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAECD0B344F FOREIGN KEY (chairman_id) REFERENCES chairman (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAEBBC049D6 FOREIGN KEY (speech_id) REFERENCES speech (id)');
        $this->addSql('ALTER TABLE weekend_program ADD CONSTRAINT FK_644901DFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE weekend_reader ADD CONSTRAINT FK_D16F89E9217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE weekend_speaker ADD CONSTRAINT FK_8D21AD3A217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAECD0B344F');
        $this->addSql('ALTER TABLE chairman DROP FOREIGN KEY FK_834095AD217BBB47');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649217BBB47');
        $this->addSql('ALTER TABLE weekend_reader DROP FOREIGN KEY FK_D16F89E9217BBB47');
        $this->addSql('ALTER TABLE weekend_speaker DROP FOREIGN KEY FK_8D21AD3A217BBB47');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAEBBC049D6');
        $this->addSql('ALTER TABLE weekend_program DROP FOREIGN KEY FK_644901DFA76ED395');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAE3EB8070A');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAE1717D737');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAED04A0F27');
        $this->addSql('DROP TABLE chairman');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE speech');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE weekend');
        $this->addSql('DROP TABLE weekend_program');
        $this->addSql('DROP TABLE weekend_reader');
        $this->addSql('DROP TABLE weekend_speaker');
    }
}
