<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210715194955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE outgoing_speech ADD speaker_id INT NOT NULL, ADD speech_id INT DEFAULT NULL, ADD congregation VARCHAR(64) NOT NULL');
        $this->addSql('ALTER TABLE outgoing_speech ADD CONSTRAINT FK_C7DBACFED04A0F27 FOREIGN KEY (speaker_id) REFERENCES weekend_speaker (id)');
        $this->addSql('ALTER TABLE outgoing_speech ADD CONSTRAINT FK_C7DBACFEBBC049D6 FOREIGN KEY (speech_id) REFERENCES speech (id)');
        $this->addSql('CREATE INDEX IDX_C7DBACFED04A0F27 ON outgoing_speech (speaker_id)');
        $this->addSql('CREATE INDEX IDX_C7DBACFEBBC049D6 ON outgoing_speech (speech_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE outgoing_speech DROP FOREIGN KEY FK_C7DBACFED04A0F27');
        $this->addSql('ALTER TABLE outgoing_speech DROP FOREIGN KEY FK_C7DBACFEBBC049D6');
        $this->addSql('DROP INDEX IDX_C7DBACFED04A0F27 ON outgoing_speech');
        $this->addSql('DROP INDEX IDX_C7DBACFEBBC049D6 ON outgoing_speech');
        $this->addSql('ALTER TABLE outgoing_speech DROP speaker_id, DROP speech_id, DROP congregation');
    }
}
