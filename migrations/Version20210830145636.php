<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210830145636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weekend_speaker DROP FOREIGN KEY FK_8D21AD3A217BBB47');
        $this->addSql('DROP INDEX UNIQ_8D21AD3A217BBB47 ON weekend_speaker');
        $this->addSql('ALTER TABLE weekend_speaker DROP person_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weekend_speaker ADD person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE weekend_speaker ADD CONSTRAINT FK_8D21AD3A217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D21AD3A217BBB47 ON weekend_speaker (person_id)');
    }
}
