<?php


namespace App\DataFixtures;


use App\Core\Entity\Person;
use App\Meeting\Weekend\Entity\Chairman;
use App\Meeting\Weekend\Entity\Speaker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;
use Symfony\Component\Dotenv\Dotenv;

class BrotherFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $em)
    {
        $csv = Reader::createFromPath(__DIR__ . '/utils/brothers.csv', 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(';');
        $result = iterator_to_array($csv->getRecords());


        for ($i = 1; $i <= count($result); $i++) {
            $person = new Person();
            $person->setFirstName($result[$i]['Prenom']);
            $person->setLastName($result[$i]['NOM']);
            $person->setGender(Person::MALE);

            if ($result[$i]['President'] == 'oui') {
                $chairman = new Chairman();
                $chairman->setPerson($person);
                $em->persist($chairman);
            }

            if ($result[$i]['Lecteur_TG'] == 'oui') {
                $reader = new \App\Meeting\Weekend\Entity\Reader();
                $reader->setPerson($person);
                $em->persist($reader);
            }

            if ($result[$i]['Orateur'] == 'oui') {
                $speaker = new Speaker();
                if ($result[$i]['Sortant'] == 'oui') {
                    $speaker->setIsOutgoing(true);
                } else {
                    $speaker->setIsOutgoing(false);
                }
                $speaker->setPerson($person);
                $em->persist($speaker);
            }

            $em->persist($person);
        }

        $em->flush();
    }

    public static function getGroups(): array
    {
        return ['brother'];
    }

}