<?php

namespace App\DataFixtures;

use App\Meeting\Weekend\Entity\Speech;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;

class SpeechFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {

        $csv = Reader::createFromPath(__DIR__ . '/utils/speeches_list.csv', 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(';');
        /** @var array $result */
        $result = iterator_to_array($csv->getRecords());

        for ($i = 1; $i < count($result); $i++) {

            $speech = new Speech();
            $speech->setTitle($result[$i]['title']);
            $speech->setNumber($result[$i]['number']);
            $speech->setCategory(strtolower($result[$i]['category']));
            $manager->persist($speech);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['speeches_list'];
    }
}
