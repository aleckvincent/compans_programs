<?php

namespace App\DataFixtures;

use App\Core\Entity\Person;
use App\Core\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixutres extends Fixture implements FixtureGroupInterface
{
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $dotenv = new Dotenv('APP_ENV');
        $admin = new Person();
        $admin->setFirstName($_ENV['ADMIN_FIRST_NAME']);
        $admin->setLastName($_ENV['ADMIN_LAST_NAME']);
        $admin->setEmail($_ENV['ADMIN_EMAIL']);
        $admin->setGender('M');
        $manager->persist($admin);

        $user = new User();

        $user->setEmail($_ENV['ADMIN_EMAIL']);
        $user->setPassword($this->encoder->encodePassword($user, $_ENV['ADMIN_PASSWORD']));
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPerson($admin);
        $manager->persist($user);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['user'];
    }
}
