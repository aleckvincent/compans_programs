<?php

namespace App\Core\Entity;

use App\Core\Repository\PersonRepository;
use App\Meeting\Weekend\Entity\Chairman;
use App\Meeting\Weekend\Entity\OutgoingSpeech;
use App\Meeting\Weekend\Entity\Reader;
use App\Meeting\Weekend\Entity\Speaker;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    const MALE = 'M';
    const FEMALE = 'F';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Ce champ ne doit pas être vide")
     * @Groups({"list", "show"})
     * @ORM\Column(type="string", length=128)
     */
    private $first_name;

    /**
     * @Assert\NotBlank(message="Ce champ ne doit pas être vide")
     * @Groups({"list", "show"})
     * @ORM\Column(type="string", length=128)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $cell_phone;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $home_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address_line_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address_line_2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address_line_3;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $baptism_day;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_baptized;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_elder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_servant;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $gender = self::MALE;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var User
     * @ORM\OneToOne(mappedBy="person", targetEntity="App\Core\Entity\User")
     */
    private $user;

    /**
     * @Groups({"list", "show"})
     * @var Speaker
     * @ORM\OneToOne(targetEntity=Speaker::class, cascade={"persist", "remove"}, mappedBy="person")
     */
    private $speaker;

    /**
     * @var Reader
     * @ORM\OneToOne(targetEntity=Reader::class, cascade={"persist", "remove"}, mappedBy="person")
     */
    private $reader;

    /**
     * @var Chairman
     * @ORM\OneToOne(targetEntity=Chairman::class, cascade={"persist", "remove"}, mappedBy="person")
     */
    private $chairman;

    /**
     * @var bool
     */
    private $isSpeaker;

    /**
     * @var bool
     */
    private $isWtReader;

    /**
     * @var bool
     */
    private $isChairman;

    /**
     * @var bool
     */
    private $isOutgoingSpeaker;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCellPhone(): ?string
    {
        return $this->cell_phone;
    }

    public function setCellPhone(?string $cell_phone): self
    {
        $this->cell_phone = $cell_phone;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->home_phone;
    }

    public function setHomePhone(?string $home_phone): self
    {
        $this->home_phone = $home_phone;

        return $this;
    }

    public function getAddressLine1(): ?string
    {
        return $this->address_line_1;
    }

    public function setAddressLine1(?string $address_line_1): self
    {
        $this->address_line_1 = $address_line_1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->address_line_2;
    }

    public function setAddressLine2(?string $address_line_2): self
    {
        $this->address_line_2 = $address_line_2;

        return $this;
    }

    public function getAddressLine3(): ?string
    {
        return $this->address_line_3;
    }

    public function setAddressLine3(?string $address_line_3): self
    {
        $this->address_line_3 = $address_line_3;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getBaptismDay(): ?\DateTimeInterface
    {
        return $this->baptism_day;
    }

    public function setBaptismDay(?\DateTimeInterface $baptism_day): self
    {
        $this->baptism_day = $baptism_day;

        return $this;
    }

    public function getIsBaptized(): ?bool
    {
        return $this->is_baptized;
    }

    public function setIsBaptized(?bool $is_baptized): self
    {
        $this->is_baptized = $is_baptized;

        return $this;
    }

    public function getIsElder(): ?bool
    {
        return $this->is_elder;
    }

    public function setIsElder(?bool $is_elder): self
    {
        $this->is_elder = $is_elder;

        return $this;
    }

    public function getIsServant(): ?bool
    {
        return $this->is_servant;
    }

    public function setIsServant(?bool $is_servant): self
    {
        $this->is_servant = $is_servant;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = self::MALE;

        return $this;
    }

    public function __toString()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Speaker
     */
    public function getSpeaker(): ?Speaker
    {
        return $this->speaker;
    }

    /**
     * @param Speaker $speaker
     */
    public function setSpeaker(Speaker $speaker): void
    {
        $this->speaker = $speaker;
    }

    /**
     * @return Reader
     */
    public function getReader(): ?Reader
    {
        return $this->reader;
    }

    /**
     * @param Reader|null $reader
     */
    public function setReader(?Reader $reader): void
    {
        $this->reader = $reader;
    }

    public function getIsSpeaker(): bool
    {
        return $this->getSpeaker() !== null ? $this->getSpeaker()->getIsActive() : false;
    }

    public function setIsSpeaker($isSpeaker)
    {
        if ($this->getSpeaker()) {
            $this->getSpeaker()->setIsActive($isSpeaker);
        } else {
            $speaker = new Speaker();
            $speaker->setPerson($this);
            $this->setSpeaker($speaker);
        }

    }

    /**
     * @return bool
     */
    public function getIsWtReader(): bool
    {
        return $this->getReader() !== null ? $this->getReader()->getIsActive() : false;
    }

    /**
     * @param bool $isWtReader
     */
    public function setIsWtReader(bool $isWtReader): void
    {
        if($this->getReader()) {
            $this->getReader()->setIsActive($isWtReader);
        } else {
            $reader = new Reader();
            $reader->setPerson($this);
            $this->setReader($reader);
        }

    }

    /**
     * @return mixed
     */
    public function getIsChairman()
    {
        return $this->getChairman() !== null ? $this->getChairman()->getIsActive() : false;
    }

    /**
     * @param mixed $isChairman
     */
    public function setIsChairman($isChairman): void
    {
        if ($this->getChairman()) {
            $this->getChairman()->setIsActive($isChairman);
        } else {
            $chairman = new Chairman();
            $chairman->setPerson($this);
            $this->setChairman($chairman);
        }

    }

    /**
     * @return mixed
     */
    public function getIsOutgoingSpeaker()
    {
        return $this->getSpeaker() !== null && $this->getSpeaker()->isOutgoing();
    }

    /**
     * @param mixed $isOutgoingSpeaker
     */
    public function setIsOutgoingSpeaker($isOutgoingSpeaker): void
    {
        $this->speaker->setIsOutgoing($isOutgoingSpeaker);

    }

    /**
     * @return Chairman
     */
    public function getChairman(): ?Chairman
    {
        return $this->chairman;
    }

    /**
     * @param Chairman $chairman
     */
    public function setChairman(Chairman $chairman): void
    {
        $this->chairman = $chairman;
    }



}
