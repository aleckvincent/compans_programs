<?php


namespace App\Core\Controller\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * Class ValidPassword
 * @package App\Core\Controller\Validator
 */
class ValidPassword extends Constraint
{
    public $wrongMessage = "Bad credential provided, please fill your existing password";

    public function validatedBy(): string
    {
        return static::class.'Validator';
    }
}