<?php


namespace App\Core\Controller\Validator;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ValidPasswordValidator extends ConstraintValidator
{
    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var TokenStorageInterface $tokenStorage
     */
    private $tokenStorage;

    /**
     * ValidPasswordValidator constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(UserPasswordEncoderInterface $encoder, TokenStorageInterface $tokenStorage)
    {
        $this->encoder = $encoder;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @param string $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidPassword) {
            throw new UnexpectedValueException($constraint, ValidPassword::class);
        }

        if (null == $value) {
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        if (!$this->encoder->isPasswordValid($user, $value)) {
            $this->context->buildViolation($constraint->wrongMessage)
                ->addViolation();
        }

    }
}