<?php


namespace App\Core\Controller;


use App\Meeting\Weekend\Entity\Program;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoreController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function index() 
    {
        return $this->redirectToRoute('dashboard');
    }


    /**
     * @Route("/dashboard", name="dashboard")
     * @return Response
     */
    public function dashboard()
    {
        $programs = $this->em->getRepository(Program::class)->findBy(
            ['draft' => false],
            ['startDate' => 'ASC']
        );
        return $this->render('dashboard.html.twig', [
            'programs' => $programs
        ]);
    }

    /**
     * @Route("/ismobile")
     * @return Response
     */
    public function isMobile()
    {
        return new Response("Il est préférable d'utiliser cette app sur un navigateur PC/Mac 😜");
    }


}