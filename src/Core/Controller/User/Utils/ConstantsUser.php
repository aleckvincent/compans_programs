<?php


namespace App\Core\Controller\User\Utils;


class ConstantsUser
{

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_VALIDATOR = 'ROLE_VALIDATOR';
    const ROLE_PUBLISHER = 'ROLE_PUBLISHER';

}