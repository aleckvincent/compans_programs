<?php


namespace App\Core\Controller\User;


use App\Core\Entity\User;
use App\Core\Form\PersonType;
use App\Core\Form\ResetPasswordType;
use App\Core\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/dashboard")
 * Class ProfileController
 * @package App\Core\Controller\User
 */
class ProfileController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * ProfileController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/me", name="me")
     * @param Request $request
     * @return Response
     */
    public function me(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(PersonType::class, $user->getPerson());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Votre compte a été mis à jour.');
            return $this->redirectToRoute('me');
        }
        return $this->render('user/profile/me.html.twig', [
            'me' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset/password", name="profile_reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return RedirectResponse|Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(ResetPasswordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->get('reset_password');
            $newPassword = $data['password']['first'];
            $encodedPassword = $encoder->encodePassword($user, $newPassword);
            $user->setPassword($encodedPassword);
            $this->em->flush();
            $this->addFlash('success', 'Mot de passe modifié');
            return $this->redirectToRoute('me');
        }
        return $this->render('user/profile/reset_password.html.twig', [
            'form' => $form->createView()
        ]);
    }


}