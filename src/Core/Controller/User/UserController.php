<?php


namespace App\Core\Controller\User;


use App\Core\Entity\User;
use App\Core\Form\UserType;
use App\Core\Security\Service\PasswordManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/users")
 * Class UserController
 * @package App\Core\Controller\User
 */
class UserController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserController constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }


    /**
     * @Route("/save", name="save_user")
     * @param Request $request
     * @param PasswordManagerInterface $passwordGenerator
     * @return RedirectResponse|Response
     */
    public function save(Request $request, PasswordManagerInterface $passwordGenerator)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordGenerator->randomPassword(8);
            $encodedPassword = $this->encoder->encodePassword($user, $password);
            $user->setPassword($encodedPassword);
            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash('success', 'Utilisateur créé, un email lui a été envoyé');
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('/user/save.html.twig', [
            'form' => $form->createView()
        ]);
    }




}