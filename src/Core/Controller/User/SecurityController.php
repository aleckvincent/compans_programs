<?php

namespace App\Core\Controller\User;

use App\Core\Entity\User;
use App\Core\Form\ResetForgotPasswordType;
use App\Core\Security\Service\PasswordManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PasswordManagerInterface
     */
    private $passwordManager;


    /**
     * SecurityController constructor.
     * @param EntityManagerInterface $em
     * @param PasswordManagerInterface $passwordManager
     */
    public function __construct(EntityManagerInterface $em, PasswordManagerInterface $passwordManager)
    {
        $this->em = $em;
        $this->passwordManager = $passwordManager;
    }


    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
             return $this->redirectToRoute('dashboard');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/forgot/password", name="forgot_password")
     * @param Request $request
     * @return Response
     */
    public function askForgotPassword(Request $request): Response
    {
        $email = $request->request->get('email');
        $error = null;
        if ($email) {
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
            if ($user) {
                $user->setForgotToken($this->passwordManager->generateToken());
                $this->em->flush();
                return $this->redirectToRoute('app_login');
            } else {
                $error = "We can not found this user. Please retry with another email or contact your Meetings Administrator";
            }
        }

        return $this->render('security/forgot_password.html.twig', [
            'error' => $error
        ]);
    }

    /**
     * @Route("/reset/password/{token}", name="reset_forgot_password")
     * @param string $token
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function resetPassword(string $token, Request $request)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['forgot_token' => $token]);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }
        $form = $this->createForm(ResetForgotPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->passwordManager->encode($user, $user->getPassword());
            $user->setPassword($password);
            $this->em->flush();
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset_forgot_password.html.twig', [
            'form' => $form->createView(),
            'token' => $token
        ]);
    }
}
