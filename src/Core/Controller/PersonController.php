<?php


namespace App\Core\Controller;


use App\Core\Entity\Person;
use App\Core\Form\PersonType;
use App\Meeting\Weekend\Service\BrotherAssignment;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/dashboard/persons")
 * Class PersonController
 * @package App\Core\Controller
 */
class PersonController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    private $em;

    private $assignmentService;

    public function __construct(EntityManagerInterface $em, BrotherAssignment $assignmentService)
    {
        $this->em = $em;
        $this->assignmentService = $assignmentService;
    }


    /**
     * @Route("/list", name="person_list")
     * @return Response
     */
    public function list(PaginatorInterface $paginator, Request $request)
    {
        /** @var Person[] $persons */
        $persons = $this->em->getRepository(Person::class)->findBy(['deletedAt' => null], ['last_name' => 'ASC']);
        $pagination = $paginator->paginate(
            $persons,
            $request->query->getInt('page', 1),
            10
        );
        $form = $this->buildForm(new Person());

        return $this->render('person/list.html.twig', [
            'persons' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="person_edit")
     * @param Person $person
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Person $person, Request $request)
    {
        $form = $this->buildForm($person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Le proclamateur a été modifié.');
            return $this->redirectToRoute('person_list');
        }

        return $this->render('person/edit.html.twig', [
            'form' => $form->createView(),
            'person' => $person
        ]);

    }

    /**
     * @Route("/delete/{id}", name="person_remove")
     * @param Person $person
     * @return RedirectResponse
     */
    public function delete(Person $person)
    {
        $person->setDeletedAt(new \DateTime());
        $this->em->flush();
        $this->addFlash('success', 'Le proclamateur a été marqué comme supprimé.');
        return $this->redirectToRoute('person_list');
    }

    private function buildForm(Person $person)
    {
        $form = $this->createForm(PersonType::class, $person);
        return $form;
    }

    /**
     * @Route("/save", name="person_save")
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function save(Request $request, ValidatorInterface $validator)
    {
        $person = new Person();
        $form = $this->buildForm($person);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($person);
            $this->em->flush();
            $this->addFlash('success', 'Un nouveau proclamateur a été ajouté.');
            return new JsonResponse('OK');
        }

        $errors = $validator->validate($person);
        $response = [];


        if (count($errors) > 0) {
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $response[] = [
                  'property' => $error->getPropertyPath(),
                  'message' => $error->getMessage()
                ];
            }
        }



        return new JsonResponse($response, Response::HTTP_BAD_REQUEST);

    }

}