<?php

namespace App\Core\Form;

use App\Core\Controller\User\Utils\ConstantsUser;
use App\Core\Entity\Person;
use App\Core\Entity\User;
use App\Core\Repository\PersonRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * @var TokenStorageInterface $tokenStorage
     */
   private $tokenStorage;

    /**
     * UserType constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [new NotBlank()]
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Validateur' => ConstantsUser::ROLE_VALIDATOR,
                    'Proclamateur' => ConstantsUser::ROLE_PUBLISHER,
                    'Administrateur' => ConstantsUser::ROLE_ADMIN
                ],
                'multiple' => true,
            ])
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'label' => 'Proclamateur',
                'query_builder' => function (EntityRepository $er) { /** @var PersonRepository $er */
                    return $er->fetchForCreation($this->tokenStorage->getToken()->getUser());
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
