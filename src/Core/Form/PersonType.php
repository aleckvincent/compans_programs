<?php

namespace App\Core\Form;

use App\Core\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', null, [
                'label' => 'Prénom',
                'required' => true
            ])
            ->add('last_name', null, [
                'label' => 'Nom',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => false
            ])
            ->add('cell_phone', null, [
                'label' => 'N° téléphone portable',
            ])
            ->add('home_phone', null, [
                'label' => 'N° téléphone domicile',
            ])
            ->add('address_line_1', null, [
                'label' => 'Adresse ligne 1',
            ])
            ->add('address_line_2', null, [
                'label' => 'Adresse ligne 2',
            ])
            ->add('address_line_3', null, [
                'label' => 'Adresse ligne 3',
            ])
            ->add('birthday', DateType::class, [
                'html5' => true,
                'widget' => 'single_text',
                'label' => 'Date de naissance',
                'required' => false
            ])
            ->add('baptism_day', DateType::class, [
                'html5' => true,
                'widget' => 'single_text',
                'label' => 'Date de baptême',
                'required' => false
            ])
            ->add('is_baptized', CheckboxType::class, [
                'label' => 'Baptisé',
                'required' => false
            ])
            ->add('is_elder', CheckboxType::class, [
                'label' => 'Ancien',
                'required' => false
            ])
            ->add('is_servant', CheckboxType::class, [
                'label' => 'Assistant',
                'required' => false
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Homme' => 'M',
                    'Femme' => 'F'
                ],
                'label' => 'Genre'
            ])
            ->add('isSpeaker', CheckboxType::class, [
                'attr' => ['class' => 'isSpeaker'],
                'mapped' => true,
                'required' => false
            ])
            ->add('isWtReader', CheckboxType::class, [
                'attr' => ['class' => 'isWtReader'],
                'mapped' => true,
                'required' => false,
            ])
            ->add('isChairman', CheckboxType::class, [
                'attr' => ['class' => 'isChairman'],
                'mapped' => true,
                'required' => false
            ])
            ->add('isOutgoingSpeaker', CheckboxType::class, [
                'attr' => ['class' => 'isOutgoingSpeaker'],
                'required' => false,
                'mapped' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
