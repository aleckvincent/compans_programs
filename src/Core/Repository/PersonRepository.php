<?php

namespace App\Core\Repository;

use App\Core\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    public function findAllForProgram()
    {
        return $this->createQueryBuilder('p')
            ->getQuery()
            ->getResult();
    }

    public function fetchForCreation(UserInterface $user)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.user', 'u')
            ->where('p.id != :id')
            ->setParameter('id', $user->getId())
            ->andWhere('u IS NULL')
            ->andWhere('p.deletedAt IS NULL')
            ->orderBy('p.last_name', 'ASC')
            ;
    }
}
