<?php


namespace App\Core\Security\Service;


use App\Core\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

interface PasswordManagerInterface
{
    /**
     * @param int $length
     * @return string
     */
    public function randomPassword(int $length) : string;

    /**
     * @return string
     */
    public function generateToken() : string;

    /**
     * @param UserInterface $user
     * @param string $plainPassword
     * @return string
     */
    public function encode(UserInterface $user, string $plainPassword) : string;

}