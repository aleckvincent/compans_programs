<?php

namespace App\Meeting\Weekend\Repository;


use App\Meeting\Weekend\Entity\Reader;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reader|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reader|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reader[]    findAll()
 * @method Reader[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReaderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reader::class);
    }

    /**
     * @return int|mixed|string
     */
    public function findAllOrderByLastTime()
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.weekends', 'w')
            ->leftJoin('r.person', 'p')
            ->where('p.deletedAt IS NULL')
            ->groupBy('p')
            ->orderBy('w.date', 'ASC');
    }
}
