<?php

namespace App\Meeting\Weekend\Repository;

use App\Meeting\Weekend\Entity\Chairman;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Chairman|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chairman|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chairman[]    findAll()
 * @method Chairman[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChairmanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chairman::class);
    }

    /**
     * @return int|mixed|string
     */
    public function findAllOrderByLastTime()
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.weekends', 'w')
            ->leftJoin('c.person', 'p')
            ->where('p.deletedAt IS NULL')
            ->groupBy('p')
            ->orderBy('w.date', 'ASC');
    }
}
