<?php

namespace App\Meeting\Weekend\Repository;


use App\Meeting\Weekend\Entity\Speech;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Speech|null find($id, $lockMode = null, $lockVersion = null)
 * @method Speech|null findOneBy(array $criteria, array $orderBy = null)
 * @method Speech[]    findAll()
 * @method Speech[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeechRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Speech::class);
    }


    /**
     * @param $key
     * @return int|mixed|string
     */
    public function autocomplete($key)
    {
        return $this->createQueryBuilder('s')
            ->select("CONCAT(s.number, ' - ', s.title, ' (', s.category, ')' ) as label, s.id as value")
            ->where('s.number LIKE :key OR s.title LIKE :key OR s.category LIKE :key')
            ->setParameter('key', '%' . $key . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function findAllOrderByLastTime()
    {
        return $this->createQueryBuilder('s')
            ->select("CONCAT(s.number, ' - ', s.title, ' (', s.category, ')' ) as label, s.id as value")
            ->leftJoin('s.weekends', 'w')
            ->groupBy('s.title')
            ->orderBy('w.date', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $id
     * @return int|mixed|string
     */
    public function findLastTime($id)
    {
        return $this->createQueryBuilder('s')
            ->select('w.date')
            ->leftJoin('s.weekends', 'w')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->orderBy('w.date', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

}
