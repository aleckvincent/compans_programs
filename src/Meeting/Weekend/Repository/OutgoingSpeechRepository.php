<?php

namespace App\Meeting\Weekend\Repository;

use App\Meeting\Weekend\Entity\OutgoingSpeech;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OutgoingSpeech|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutgoingSpeech|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutgoingSpeech[]    findAll()
 * @method OutgoingSpeech[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutgoingSpeechRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutgoingSpeech::class);
    }

    // /**
    //  * @return OutgoingSpeech[] Returns an array of OutgoingSpeech objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OutgoingSpeech
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
