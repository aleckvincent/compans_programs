<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\SpeakerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Core\Entity\Person;

/**
 * @ORM\Table(name="weekend_speaker")
 * @ORM\Entity(repositoryClass=SpeakerRepository::class)
 */
class Speaker
{
    /**
     * @Groups({"list"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"list", "show"})
     * @var Person
     * @ORM\OneToOne(targetEntity=Person::class, cascade={"persist", "remove"})
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity=Weekend::class, mappedBy="speaker")
     */
    private $weekends;

    /**
     * @var boolean
     * @ORM\Column(name="is_outgoing", type="boolean")
     */
    private $isOutgoing = false;

    /**
     * @var OutgoingSpeech
     * @ORM\OneToOne(targetEntity=OutgoingSpeech::class, cascade={"persist", "remove"}, mappedBy="speaker")
     */
    private $outgoingSpeech;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|Weekend[]
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setSpeaker($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getSpeaker() === $this) {
                $weekend->setSpeaker(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isOutgoing(): ?bool
    {
        return $this->isOutgoing;
    }

    /**
     * @param bool $isOutgoing
     */
    public function setIsOutgoing(bool $isOutgoing): void
    {
        $this->isOutgoing = $isOutgoing;
    }



    public function __toString()
    {
        return $this->person->getFirstName() . ' ' . $this->person->getLastName();
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return OutgoingSpeech
     */
    public function getOutgoingSpeech(): ?OutgoingSpeech
    {
        return $this->outgoingSpeech;
    }

    /**
     * @param OutgoingSpeech $outgoingSpeech
     */
    public function setOutgoingSpeech(OutgoingSpeech $outgoingSpeech): void
    {
        $this->outgoingSpeech = $outgoingSpeech;
    }




}
