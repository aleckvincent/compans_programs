<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\SpeechRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=SpeechRepository::class)
 */
class Speech
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="string", nullable=true, length=128)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=Weekend::class, mappedBy="speech")
     */
    private $weekends;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function __toString()
    {
        return $this->number . ' - ' . $this->title;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Weekend[]
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setSpeech($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getSpeech() === $this) {
                $weekend->setSpeech(null);
            }
        }

        return $this;
    }
}
