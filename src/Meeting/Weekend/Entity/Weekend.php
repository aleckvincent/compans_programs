<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\WeekendRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Meeting\Weekend\Validator as MeetingWeekendValidator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MeetingWeekendValidator\IsValid()
 * @ORM\Table(name="weekend")
 * @ORM\Entity(repositoryClass=WeekendRepository::class)
 */
class Weekend
{
    const TYPE_OVERSEER = 'overseer';
    const TYPE_EXTERNAL_SPEAKER = 'external_speaker';
    const TYPE_CONVENTION = 'convention';
    const TYPE_OUTGOING_SPEAKER = 'outgoing';
    const TYPE_CONVENTION_FRIDAY_MORNING = 'convention_friday_morning';
    const TYPE_CONVENTION_FRIDAY_AFTERNOON = 'convention_friday_afternoon';
    const TYPE_CONVENTION_SATURDAY_MORNING = 'convention_saturday_morning';
    const TYPE_CONVENTION_SATURDAY_AFTERNOON = 'convention_saturday_afternoon';
    const TYPE_CONVENTION_SUNDAY_MORNING = 'convention_sunday_morning';
    const TYPE_CONVENTION_SUNDAY_AFTERNOON = 'convention_sunday_afternoon';

    /**
     * @Groups({"show"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Program::class, inversedBy="weekends", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $program;

    /**
     * @Groups({"show"})
     * @var Speaker
     * @ORM\ManyToOne(targetEntity=Speaker::class, inversedBy="weekends")
     * @ORM\JoinColumn(nullable=true)
     */
    private $speaker;

    /**
     * @Groups({"show"})
     * @var Reader
     * @ORM\ManyToOne(targetEntity=Reader::class, inversedBy="weekends")
     * @ORM\JoinColumn(nullable=true)
     */
    private $reader;

    /**
     * @Groups({"show"})
     * @var Chairman
     * @ORM\ManyToOne(targetEntity=Chairman::class, inversedBy="weekends")
     * @ORM\JoinColumn(nullable=true)
     */
    private $chairman;

    /**
     * @Groups({"show"})
     * @var Speech
     * @ORM\ManyToOne(targetEntity=Speech::class, inversedBy="weekends")
     * @ORM\JoinColumn(nullable=true)
     */
    private $speech;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $external_name;


    /**
     * @Groups({"show"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $overseer_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $type;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity=OutgoingSpeech::class, mappedBy="weekend", cascade={"persist"})
     */
    private $outgoingSpeeches;

    public function __construct()
    {
        $this->outgoingSpeeches = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProgram(): ?Program
    {
        return $this->program;
    }

    public function setProgram(?Program $program): self
    {
        $this->program = $program;

        return $this;
    }

    public function getSpeaker(): ?Speaker
    {
        return $this->speaker;
    }

    public function setSpeaker(?Speaker $speaker): self
    {
        if ($this->type != self::TYPE_EXTERNAL_SPEAKER || $this->type != self::TYPE_OVERSEER) {
            $this->speaker = $speaker;
        }


        return $this;
    }

    public function getReader(): ?Reader
    {
        return $this->reader;
    }

    public function setReader(?Reader $reader): self
    {
        $this->reader = $reader;

        return $this;
    }

    /**
     * @return Chairman|null
     */
    public function getChairman(): ?Chairman
    {
        return $this->chairman;
    }

    public function setChairman(?Chairman $chairman): self
    {
        $this->chairman = $chairman;

        return $this;
    }

    public function getSpeech(): ?Speech
    {
        return $this->speech;
    }

    public function setSpeech(?Speech $speech): self
    {
        $this->speech = $speech;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getExternalName(): ?string
    {
        return $this->external_name;
    }

    public function setExternalName(?string $external_name): self
    {
        $this->external_name = $external_name;

        return $this;
    }


    public function getOverseerName(): ?string
    {
        return $this->overseer_name;
    }

    public function setOverseerName(?string $overseer_name): self
    {
        $this->overseer_name = $overseer_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Collection|OutgoingSpeech[]
     */
    public function getOutgoingSpeeches(): Collection
    {
        return $this->outgoingSpeeches;
    }

    public function addOutgoingSpeech(OutgoingSpeech $outgoingSpeech): self
    {
        if (!$this->outgoingSpeeches->contains($outgoingSpeech)) {
            $this->outgoingSpeeches[] = $outgoingSpeech;
            $outgoingSpeech->setWeekend($this);
        }

        return $this;
    }

    public function removeOutgoingSpeech(OutgoingSpeech $outgoingSpeech): self
    {
        if ($this->outgoingSpeeches->removeElement($outgoingSpeech)) {
            // set the owning side to null (unless already changed)
            if ($outgoingSpeech->getWeekend() === $this) {
                $outgoingSpeech->setWeekend(null);
            }
        }

        return $this;
    }
}
