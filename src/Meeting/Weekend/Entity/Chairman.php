<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\ChairmanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Core\Entity\Person;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ChairmanRepository::class)
 */
class Chairman
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"show"})
     * @ORM\OneToOne(targetEntity=Person::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity=Weekend::class, mappedBy="chairman")
     */
    private $weekends;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|Weekend[]
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setChairman($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getChairman() === $this) {
                $weekend->setChairman(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->person->getFirstName() . ' ' . $this->person->getLastName();
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }


}
