<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\ProgramRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Core\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="weekend_program")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ProgramRepository::class)
 */
class Program
{
    const VALIDATED = 2;
    const DRAFT = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Valid()
     * @Groups({"show"})
     * @ORM\OneToMany(targetEntity=Weekend::class, mappedBy="program", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $weekends;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @Groups({"show"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    /**
     * @ORM\Column(type="boolean")
     */
    private $draft = false;


    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $user;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return Collection|Weekend[]
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setProgram($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getProgram() === $this) {
                $weekend->setProgram(null);
            }
        }

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->draft;
    }

    /**
     * @param bool $draft
     */
    public function setDraft(bool $draft): void
    {
        $this->draft = $draft;
    }



}
