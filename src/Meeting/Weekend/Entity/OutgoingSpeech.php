<?php

namespace App\Meeting\Weekend\Entity;

use App\Meeting\Weekend\Repository\OutgoingSpeechRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OutgoingSpeechRepository::class)
 */
class OutgoingSpeech
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Weekend::class, inversedBy="outgoingSpeeches")
     */
    private $weekend;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity=Speaker::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $speaker;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity=Speech::class)
     */
    private $speech;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=64)
     */
    private $congregation;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeekend(): ?Weekend
    {
        return $this->weekend;
    }

    public function setWeekend(?Weekend $weekend): self
    {
        $this->weekend = $weekend;

        return $this;
    }

    public function getSpeaker(): ?Speaker
    {
        return $this->speaker;
    }

    public function setSpeaker(?Speaker $speaker): self
    {
        $this->speaker = $speaker;

        return $this;
    }

    public function getSpeech(): ?Speech
    {
        return $this->speech;
    }

    public function setSpeech(?Speech $speech): self
    {
        $this->speech = $speech;

        return $this;
    }

    public function getCongregation(): ?string
    {
        return $this->congregation;
    }

    public function setCongregation(?string $congregation): self
    {
        $this->congregation = $congregation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }


}
