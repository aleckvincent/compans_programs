<?php


namespace App\Meeting\Weekend\Model;



use App\Meeting\Weekend\Entity\Program;
use App\Meeting\Weekend\Entity\Weekend;
use App\Meeting\Weekend\Service\DateCalculation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ProgramManager
{
    /**
     * @var DateCalculation
     */
    private $dateCalculation;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param DateCalculation $dateCalculation
     * @param EntityManagerInterface $em
     */
    public function __construct(DateCalculation $dateCalculation, EntityManagerInterface $em)
    {
        $this->dateCalculation = $dateCalculation;
        $this->em = $em;
    }


    /**
     * @param Weekend[] $contents
     */
    public function setFieldsAsNull(iterable $contents): void
    {
        foreach ($contents as $content) {
            if ($content->getType() == Weekend::TYPE_CONVENTION) {
                $content->setReader(null);
                $content->setSpeaker(null);
                $content->setChairman(null);
                $content->setSpeech(null);
            }
            if ($content->getType() == Weekend::TYPE_OVERSEER) {
                $content->setReader(null);
                $content->setSpeaker(null);
                $content->setSpeech(null);
            }
            if ($content->getType() == Weekend::TYPE_EXTERNAL_SPEAKER) {
                $content->setSpeaker(null);
            }
        }
    }

    /**
     * @param iterable $contents
     * @return array
     * @throws Exception
     */
    public function getStartAndEndDate(iterable $contents)
    {
        // define start and end date
        $length = $contents->count();

        foreach ($contents as $content) {
            if ($content->getDate() != null) {
                $startDate = $content->getDate();
                break;
            }
        }

        $date = new \DateTime($startDate->format('Y-m-d'));
        $startDate = $date;

        if ($length == 1) {
            $endDate = $startDate;
        } else {
            $endDate = $contents->get($length-1)->getDate();
        }

        $result = [];
        $result['startDate'] = $startDate;
        $result['endDate'] = $endDate;

        return $result;
    }

    public function persist(Program $program, bool $draft, UserInterface $user)
    {
        $this->setFieldsAsNull($program->getWeekends()); // set fields as null if it's convention, external speaker or overseer week
        $dates = $this->getStartAndEndDate($program->getWeekends()); // define start and end date
        $program->setStartDate($dates['startDate']);
        $program->setEndDate($dates['endDate']);
        $program->setUser($user);
        $program->setDraft($draft);
        $this->em->persist($program);
        $this->em->flush();
    }
}