<?php

namespace App\Meeting\Weekend\Service;

use App\Core\Entity\Person;
use App\Meeting\Weekend\Entity\Reader;
use App\Meeting\Weekend\Entity\Speaker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\InputBag;

class BrotherAssignment
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Person $person
     */
    public function setReader(Person $person)
    {
        if (!$person->getReader()) {
            $reader = new Reader();
            $reader->setPerson($person);
        } else {
            $person->getReader()->setIsActive(true);
        }
    }

    /**
     * @param Person $person
     */
    public function removeReader(Person $person) : void
    {
        if ($person->getReader()) {
            $person->getReader()->setIsActive(false);
            dump($person->getReader()->getIsActive());
            $this->em->persist($person->getReader());
            $this->em->flush();
        }
    }

    /**
     * Check if the publisher is speaker or no
     * @param Person $person
     * @param bool $check
     */
    public function checkSpeaker(Person $person, bool $check)
    {
        if ($check == true) {
            if (!$person->getSpeaker()) {
                $speaker = new Speaker();
                $speaker->setPerson($person);
                $speaker->setIsActive(true);
            } else {
                $person->getSpeaker()->setIsActive(true);
            }
        } else {
            if ($person->getSpeaker()) {
                $person->getSpeaker()->setIsActive(false);
            }
        }
    }


    /**
     * @param FormInterface $postData
     * @param Person $person
     */
    public function handle(FormInterface $postData, Person $person)
    {


        $isSpeaker = $postData['isSpeaker']->getData();
        $isWtReader = $postData['isWtReader']->getData();



        if ($isWtReader) {
            $this->setReader($person);
        }
        if (!$isWtReader) {
            $this->removeReader($person);

        }

        if ($isSpeaker) {
            $this->checkSpeaker($person, $isSpeaker);
        }

    }

}
