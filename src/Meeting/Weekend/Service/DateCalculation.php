<?php


namespace App\Meeting\Weekend\Service;


use Symfony\Component\Serializer\SerializerInterface;

class DateCalculation
{
    /**
     * @var SerializerInterface $serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param int $month
     * @param int $year
     * @return \DatePeriod
     */
    public function getSaturdaysPeriod(int $month, int $year) : \DatePeriod
    {
        if ($month !== 12) {
            $nextMonth = $month + 1;
            $nextYear = $year;
        } else {
            $nextMonth = 01;
            $nextYear = $year + 1;
        }

        return new \DatePeriod(
            new \DateTime("first saturday of $year-$month"),
            \DateInterval::createFromDateString('next saturday'),
            new \DateTime("first day of $nextYear-$nextMonth")
        );
    }

    /**
     * @param int $month
     * @param int $year
     * @return array
     */
    public function getAllSaturdays(int $month, int $year) : array
    {
        $period = $this->getSaturdaysPeriod($month, $year);
        $dates = [];
        foreach ($period as $key => $value) {
            $dates[] =  $value->format('d/m/Y')
            ;
        }

        return $dates;
    }

}
