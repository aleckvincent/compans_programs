<?php


namespace App\Meeting\Weekend\Controller;


use App\Meeting\Weekend\Entity\Program;
use App\Meeting\Weekend\Form\ProgramType;
use App\Meeting\Weekend\Model\ProgramManager;
use App\Meeting\Weekend\Service\DateCalculation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/programs")
 * Class ProgramController
 */
class ProgramController extends AbstractController
{

    private $em;
    /**
     * @var ProgramManager
     */
    private $manager;

    /**
     * @var DateCalculation
     */
    private $dateCalculation;

    public function __construct(EntityManagerInterface $em, ProgramManager $manager, DateCalculation $dateCalculation)
    {
        $this->em = $em;
        $this->manager = $manager;
        $this->dateCalculation = $dateCalculation;
    }

    /**
     * @Route("/save", name="program_save")
     * @param Request $request
     * @return Response
     */
    public function save(Request $request)
    {
        $month = intval($request->query->get('month'));
        $year = intval($request->query->get('year'));
        $saturdays = $this->dateCalculation->getAllSaturdays($month, $year);
        $program = new Program();
        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($request->query->get('draft')) {
                $this->manager->persist($program, true, $this->getUser());
                $this->addFlash('success', 'Programme sauvegardé');
                return $this->redirectToRoute('program_list');
            }

            if ($form->isValid()) {
                $this->manager->persist($program, false, $this->getUser());
                $this->addFlash('success', 'Programme créé');
                return $this->redirectToRoute('program_list');
            }
        }

        return $this->render('meeting/weekend/program/save.html.twig', [
            'form' => $form->createView(),
            'nb_saturdays' => count($saturdays),
            'saturdays' => $saturdays,
        ]);
    }

    /**
     * @Route("/", name="program_list")
     * @return Response
     */
    public function list()
    {
        $repo = $this->getDoctrine()->getRepository(Program::class);

        return $this->render('meeting/weekend/program/list.html.twig', [
            'programs' => $repo->findBy([], ['startDate' => 'ASC'])
        ]);
    }


    /**
     * @Route("/show/{id}", name="program_show")
     * @param Pdf $knpPDF
     * @param Program $program
     * @param Request $request
     * @return Response
     */
    public function show(Pdf $knpPDF, Program $program, Request $request)
    {
        $isPDF = $request->get('pdf');

        if ($isPDF) {
            $timestamp = new \DateTime();
            $html = $this->renderView('meeting/weekend/program/show_pdf.html.twig', [
                'p' => $program
            ]);
            return new PdfResponse($knpPDF->getOutputFromHtml($html), 'program_' . $timestamp->format('dmyHis') . '.pdf');
        }

        if ($request->isXmlHttpRequest()) {
            return $this->render('meeting/weekend/program/_show_ajax.html.twig', [
                'program' => $program
            ]);
        }

        return $this->render('meeting/weekend/program/show.html.twig', [
            'program' => $program
        ]);

    }


    /**
     * @Route("/edit/{id}", name="program_edit")
     * @param Program $program
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Program $program, Request $request)
    {
        $originalProgram = new ArrayCollection();

        foreach ($program->getWeekends() as $content) {
            $originalProgram->add($content);
        }

        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if ($request->query->get('draft')) {
                $this->manager->persist($program, true, $this->getUser());
                $this->addFlash('success', 'Programme sauvegardé');
                return $this->redirectToRoute('program_list');
            }
            if ($form->isValid()) {
                $this->manager->persist($program, false, $this->getUser());
                $this->addFlash('success', 'Programme modifié');
                return $this->redirectToRoute('program_list');
            }
        }

        return $this->render('meeting/weekend/program/edit.html.twig', [
            'form' => $form->createView(),
            'program' => $program
        ]);
    }

    /**
     * @Route("/remove/{id}", name="program_remove")
     * @param Program $program
     */
    public function remove(Program $program)
    {
        $this->em->remove($program);
        $this->em->flush();
        $this->addFlash('success', 'Programme supprimé');
        return $this->redirectToRoute('program_list');
    }


    /**
     * @Route("/validate/{id}", name="program_validate")
     * @param Program $program
     * @return RedirectResponse
     */
    public function validate(Program $program)
    {
        $program->setStatus(Program::VALIDATED);
        $this->em->flush();
        $this->addFlash('notice', 'Le programme a été validé.');
        return $this->redirectToRoute('program_list');

    }

}
