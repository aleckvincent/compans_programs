<?php


namespace App\Meeting\Weekend\Controller;


use App\Meeting\Weekend\Entity\Program;
use App\Meeting\Weekend\Entity\Weekend;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RestProgramController extends AbstractController
{
    /** @var SerializerInterface $serializer */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @Route("/rest/programs/last-date")
     * @return Response
     */
    public function getLastDate()
    {
        $date = $this->getDoctrine()->getRepository(Weekend::class)->getLastDate();

        if (!isset($date['date'])) {
            $date = new \DateTime();
            $dateFormatStr = $date->format('Y-m-d');
        } else {
            $date['date']->add(new \DateInterval('P7D'));
            $dateFormatStr = $date['date']->format('Y-m-d');
        }

        $json = $this->serializer->serialize($dateFormatStr, 'json');
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * @Route("/rest/programs/show/{id}")
     * @param Program $program
     * @return Response
     */
    public function showProgramById(Program $program)
    {
        $json = $this->serializer->serialize($program,
            'json',
            [
                'groups' => 'show'
            ]
        );
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}