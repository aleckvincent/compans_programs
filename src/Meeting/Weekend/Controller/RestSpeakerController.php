<?php


namespace App\Meeting\Weekend\Controller;


use App\Meeting\Weekend\Entity\Speaker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RestSpeakerController extends AbstractController
{

    /**
     * @Route("/rest/list")
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function list(SerializerInterface $serializer)
    {
        /** @var Speaker[] $speaker */
        $speaker = $this->getDoctrine()->getRepository(Speaker::class)->findAll();
        $json = $serializer->serialize($speaker, 'json', [
            'groups' => 'list'
        ]);
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



}