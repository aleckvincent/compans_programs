<?php


namespace App\Meeting\Weekend\Controller;


use App\Meeting\Weekend\Entity\Speech;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RestSpeechController extends AbstractController
{
    /** @var SerializerInterface $serializer */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @Route("/rest/speech/autocomplete")
     * @param Request $request
     * @return Response
     */
    public function autocomplete(Request $request)
    {
        if ($request->get('key')) {
            $key = $request->get('key');
            $data = $this->getDoctrine()->getRepository(Speech::class)->autocomplete($key);
        } else {
            $data = $this->getDoctrine()->getRepository(Speech::class)->findAllOrderByLastTime();
        }

        $json = $this->serializer->serialize($data, 'json');
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/rest/speech/show/{id}")
     * @param Speech $speech
     * @return Response
     */
    public function show(Speech $speech)
    {
        $json = $this->serializer->serialize($speech, 'json');

        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/rest/speech/last-time/{id}")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function fetchLastTime($id)
    {
        $speechLastTime = $this->getDoctrine()->getRepository(Speech::class)->findLastTime($id);
        $timestamp = [];
        if ($speechLastTime['date']) {
            /** @var \DateTime $date */
            $date = $speechLastTime['date'];
            $timestamp = [
                'timestamp' => $date->getTimestamp(),
                'month' => $date->format('n'),
                'date' => $date->format('m/d/Y'),
                'dateFr' => $date->format('d/m/Y')
            ];
        }

        $json = $this->serializer->serialize($timestamp, 'json');

        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}