<?php


namespace App\Meeting\Weekend\Validator;


use App\Meeting\Weekend\Entity\Weekend;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class IsValidValidator extends ConstraintValidator
{

    /**
     * @param Weekend $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsValid) {
            throw new UnexpectedValueException($constraint, IsValid::class);
        }

        if (null == $value || '' === $value) {
            return;
        }

        if (null === $value->getDate()) {
            $this->context->buildViolation($constraint->dateBlank)
                ->atPath('date')
                ->addViolation();
        }

        if ($value->getType() == Weekend::TYPE_EXTERNAL_SPEAKER) {
            if (null == $value->getExternalName() || '' === $value->getExternalName()) {
                $this->context->buildViolation($constraint->externalNameMsg)
                    ->atPath('external_name')
                    ->addViolation();
            }
        }

        if ($value->getType() == Weekend::TYPE_OVERSEER) {
            if (null == $value->getOverseerName() || '' === $value->getOverseerName()) {
                $this->context->buildViolation($constraint->overseerNameMsg)
                    ->atPath('overseer_name')
                    ->addViolation();
            }
        }

        if ($value->getType() != (Weekend::TYPE_CONVENTION || Weekend::TYPE_CONVENTION_FRIDAY_AFTERNOON || Weekend::TYPE_CONVENTION_FRIDAY_MORNING || Weekend::TYPE_CONVENTION_SATURDAY_AFTERNOON || Weekend::TYPE_CONVENTION_SATURDAY_MORNING || Weekend::TYPE_CONVENTION_SUNDAY_AFTERNOON || Weekend::TYPE_CONVENTION_SUNDAY_MORNING )) {
            if (null == $value->getReader() && $value->getType() != Weekend::TYPE_OVERSEER) {
                $this->context->buildViolation($constraint->notBlank)
                    ->atPath('reader')
                    ->addViolation();
            }
        }

        if ($value->getType() != (Weekend::TYPE_CONVENTION || Weekend::TYPE_CONVENTION_FRIDAY_AFTERNOON || Weekend::TYPE_CONVENTION_FRIDAY_MORNING || Weekend::TYPE_CONVENTION_SATURDAY_AFTERNOON || Weekend::TYPE_CONVENTION_SATURDAY_MORNING || Weekend::TYPE_CONVENTION_SUNDAY_AFTERNOON || Weekend::TYPE_CONVENTION_SUNDAY_MORNING )) {
            if (null == $value->getChairman()) {
                $this->context->buildViolation($constraint->notBlank)
                    ->atPath('chairman')
                    ->addViolation();
            }
            if (null == $value->getSpeech() && $value->getType() != Weekend::TYPE_OVERSEER) {
                $this->context->buildViolation($constraint->notBlank)
                    ->atPath('speech')
                    ->addViolation();
            }

            if (null == $value->getSpeaker() && $value->getType() != Weekend::TYPE_OVERSEER && $value->getType() != Weekend::TYPE_EXTERNAL_SPEAKER) {
                $this->context->buildViolation($constraint->speakerMsg)
                    ->atPath('speaker')
                    ->addViolation();
            }

        }
    }
}
