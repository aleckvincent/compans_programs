<?php


namespace App\Meeting\Weekend\Validator;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * Class IsValid
 * @package App\Meeting\Weekend\Validator
 */
class IsValid extends Constraint
{
    public $externalNameMsg = "The external speaker's name should not be blank";
    public $overseerNameMsg = "The overseer's name should not be blank";
    public $speakerMsg = "Please select a speaker";
    public $notBlank = "This value should not be blank";
    public $dateBlank = "Please add a date";

    public function validatedBy()
    {
        return static::class.'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}