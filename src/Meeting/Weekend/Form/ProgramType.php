<?php

namespace App\Meeting\Weekend\Form;

use App\Meeting\Weekend\Entity\Program;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('weekends', CollectionType::class, [
            'entry_type' => WeekendType::class,
            'allow_add' => true,
            'entry_options' => ['label' => false],
            'by_reference' => false,
            'allow_delete' => true
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Program::class,
        ]);
    }
}
