<?php

namespace App\Meeting\Weekend\Form;

use App\Meeting\Weekend\Entity\Chairman;
use App\Meeting\Weekend\Entity\OutgoingSpeech;
use App\Meeting\Weekend\Entity\Reader;
use App\Meeting\Weekend\Entity\Speaker;
use App\Meeting\Weekend\Entity\Speech;
use App\Meeting\Weekend\Entity\Weekend;
use App\Meeting\Weekend\Repository\ChairmanRepository;
use App\Meeting\Weekend\Repository\ReaderRepository;
use App\Meeting\Weekend\Repository\SpeakerRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'html5' => false,
                'attr' => [
                    'class' => 'datepicker ',
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])
            ->add('speaker', EntityType::class, [
                'class' => Speaker::class,
                'query_builder' => function (EntityRepository $er) { /** @var SpeakerRepository $er */
                    return $er->findAllOrderByLastTime();
                },
                'attr' => [
                    'class' => ' input-speaker'
                ],
                'placeholder' => 'Choix de l\'orateur',
                'label' => 'Orateur',
                'required' => false
            ])
            ->add('reader', EntityType::class, [
                'class' => Reader::class,
                'query_builder' => function (EntityRepository $er) { /** @var ReaderRepository $er */
                    return $er->findAllOrderByLastTime()
                        ;
                },
                'attr' => [
                    'class' => ' input-reader input-select2'
                ],
                'placeholder' => 'Choix du lecteur',
                'label' => 'Lecteur',
                'required' => false
            ])
            ->add('chairman', EntityType::class, [
                'class' => Chairman::class,
                'query_builder' => function (EntityRepository $er) { /** @var ChairmanRepository $er */
                    return $er->findAllOrderByLastTime()
                        ;
                },
                'attr' => [
                    'class' => ' input-chairman input-select2'
                ],
                'placeholder' => 'Choix du président',
                'label' => 'Président',
                'required' => false
            ])
            ->add('speech', EntityType::class, [
                'class' => Speech::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s');
                },
                'attr' => [
                    'class' => 'input-speech'
                ],
                'placeholder' => 'Choix du discours',
                'label' => 'Discours',
                'required' => false
            ])
            ->add('external_name', TextType::class, [

                'attr' => [
                    'class' => 'external-name hidden',
                    'placeholder' => 'Saisis le nom de l\'orateur',
                    'required' => false
                ]
            ])
            ->add('overseer_name', TextType::class, [
                'attr' => [
                    'class' => 'overseer-name hidden',
                    'placeholder' => 'Saisis le nom du responsable de circonscription',
                    'required' => false
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Réunion classique' => null,
                    'Assemblée' => Weekend::TYPE_CONVENTION,
                    'Orateur externe' => Weekend::TYPE_EXTERNAL_SPEAKER,
                    'Visite C/O' => Weekend::TYPE_OVERSEER,
                    'Assemblée régionale - Vendredi matin' => Weekend::TYPE_CONVENTION_FRIDAY_MORNING,
                    'Assemblée régionale - Vendredi après midi' => Weekend::TYPE_CONVENTION_FRIDAY_AFTERNOON,
                    'Assemblée régionale - Samedi matin' => Weekend::TYPE_CONVENTION_SATURDAY_MORNING,
                    'Assemblée régionale - Samedi après midi' => Weekend::TYPE_CONVENTION_SATURDAY_AFTERNOON,
                    'Assemblée régionale - Dimanche matin' => Weekend::TYPE_CONVENTION_SUNDAY_MORNING,
                    'Assemblée régionale - Dimanche après midi' => Weekend::TYPE_CONVENTION_SUNDAY_AFTERNOON,
                ],
                'attr' => [
                    'class' => 'choice-type'
                ]
            ])
            ->add('outgoing', CollectionType::class, [
                'entry_type' => OutgoingSpeechType::class,
                'allow_add' => true,
                'entry_options' => ['label' => true],
                'by_reference' => false,
                'allow_delete' => true,
                'prototype_name' => '__index__',
                'property_path' => 'outgoingSpeeches'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Weekend::class,
        ]);
    }
}
