<?php

namespace App\Meeting\Weekend\Form;

use App\Meeting\Weekend\Entity\OutgoingSpeech;
use App\Meeting\Weekend\Entity\Speech;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OutgoingSpeechType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('congregation', TextType::class, [
                'attr' => [
                    'class' => 'congregation',
                    'data-placeholder' => "Select a state"
                ]
            ])
            ->add('speaker', null, [
                'attr' => [
                    'class' => 'select-speaker-outgoing js-custom-select'
                ],
                'placeholder' => 'Choix de l\'orateur'
            ])
            ->add('speech', null, [
                'attr' => [
                    'class' => 'select-speech-outgoing js-custom-select'
                ],
                'placeholder' => 'Choix du discours'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OutgoingSpeech::class,
        ]);
    }
}
