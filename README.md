# Meetings

⚠ Projet en phase de développement.

##Installation

Configuration serveur requise :
- PHP >= 7.4
- MySQL 5.7
- Symfony
- wkhtmltopdf (https://wkhtmltopdf.org/downloads.html)

Une fois dans à la racine du projet.

Installations des dépendances composer

```bash
$ composer install
```

Installation des dépendances yarn
```bash
$ yarn install
```
Copiez le fichier `.env` vers `.env.local` et editer `DATABASE_URL` selon votre configuration (username, passsord, database_name)

⚠ Pensez à modifier les informations suivante :
* `CONGREGATION_NAME`
* `ADMIN_EMAIL`
* `ADMIN_PASSWORD`
* `ADMIN_FIRST_NAME`
* `ADMIN_LAST_NAME`
* `WKHTMLTOPDF_PATH`

Pour créer la base de données :

```bash
$ php bin/console doctrine:dabatase:create
```

Ensuite lancer les migrations et fixtures :

```shell
$ php bin/console doctrine:migrations:migrate
```
```shell
$ php bin/console doctrine:fixtures::load
```
🎉 Ready to code !
