$(document).ready(function() {

    $('.js-custom-select').select2();

    $('.add-speaker').click(function (e) {
        e.preventDefault();
        let weekendId = $(this).data('weekend-id');
        let $collectionHolder = $('div[data-weekend-id= ' + weekendId + ']');
        $collectionHolder.data('index', $collectionHolder.find('input').length);
        addFormToCollection($collectionHolder);
        $('.js-custom-select').select2();
    })

    removeItem();


    function addFormToCollection($collectionHolder)
    {
        let prototype = $collectionHolder.data('prototype');
        let index = $collectionHolder.data('index');

        let newForm = prototype;
        newForm = newForm.replace(/__index__/g, index);

        $collectionHolder.data('index', index + 1);
        let $newFormSection = $collectionHolder.append(newForm);
        $collectionHolder.append($newFormSection);
        removeItem(); // add event listener to allow item to be removed
    }

    function removeItem() {
        $('.remove-outgoing-speaker').click(function (e) {
            e.preventDefault();
            let $inputGroupParent = $(this).parent();
            $inputGroupParent.remove();
        })
    }

    $('.input-group').map(function () {
        let valid = $(this).data('valid');
        let id = $(this).data('index');
        if (!valid) {
            $('.outgoing-link-' + id).addClass('text-danger');
        }
    })




});