$(document).ready(function () {

    const $selectMonth = $('#month');
    const $selectYear = $('#year');
    const $submit = $('#validate');
    let month = null;
    let year = null;

    $selectMonth.change(function () {
        month = $(this).val();
    });

    $selectYear.change(function () {
        year = $(this).val();
        $submit.attr('href', '/dashboard/programs/save?' + 'month=' + month + '&year=' + year);
    });

})