let $formPrograms = $('#formPrograms');
const isSaveView = $formPrograms.data('view') === 'save';
const numberSaturdays = parseInt($formPrograms.data('number-saturdays'));
const saturdays = $formPrograms.data('saturdays');
const year = $formPrograms.data('year');
const month = $formPrograms.data('month');
const OVERSEER = $formPrograms.data('overseer');
const EXTERNAL_SPEAKER = $formPrograms.data('external-speaker');
const CONVENTION = $formPrograms.data('convention');

initForm()




/**
 * Manage Collection type form with prototype
 */
function initForm() {

    jQuery(document).ready(function() {
        // Get the ul that holds the collection of talks contents
        var $contentCollectionHolder = $('.programs');
        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $contentCollectionHolder.data('index', $contentCollectionHolder.find('input').length);

        if (isSaveView) {
            for (let i = 0; i < numberSaturdays; i++) {
                addFormToCollection($contentCollectionHolder, saturdays[i]);
            }
        } else {
            loadEditView();
        }

        submitForm($('#formPrograms'));

    });
}

/**
 * Add a new  collection to the form
 * @param $collectionHolder
 * @param saturdayDate
 */
function addFormToCollection($collectionHolder, saturdayDate) {

    // Get the data-prototype explained earlier
    let prototype = $collectionHolder.data('prototype');

    // get the new index
    let index = $collectionHolder.data('index');

    let newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    const $programs = $('.programs');

    let $newFormSection = $programs.append(newForm);
    $collectionHolder.append($newFormSection);

    let inputSpeech = $collectionHolder.find('#program_weekends_' + index + '_speech')
    let inputDatepicker = $collectionHolder.find('#program_weekends_' + index + '_date')
    let inputExternalName = $collectionHolder.find('#program_weekends_' + index + '_external_name')
    let inputSpeaker = $collectionHolder.find('#program_weekends_' + index + '_speaker')
    let inputOverseerName = $collectionHolder.find('#program_weekends_' + index + '_overseer_name')
    let inputReader = $collectionHolder.find('#program_weekends_' + index + '_reader')
    let $talkReader = $collectionHolder.find('.talk_reader' + index)
    let inputChairman = $collectionHolder.find('#program_weekends_' + index + '_chairman')
    let $dateTextHolder = $(inputDatepicker).closest('tr').find('.date-text');
    let $typeWeekend = $collectionHolder.find('#program_weekends_' + index + '_type');

    $(inputDatepicker).datepicker({
        closeText: "Fermer",
        prevText: "Précédent",
        nextText: "Suivant",
        currentText: "Aujourd'hui",
        monthNames: [ "janvier", "février", "mars", "avril", "mai", "juin",
            "juillet", "août", "septembre", "octobre", "novembre", "décembre" ],
        monthNamesShort: [ "janv.", "févr.", "mars", "avr.", "mai", "juin",
            "juil.", "août", "sept.", "oct.", "nov.", "déc." ],
        dayNames: [ "dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi" ],
        dayNamesShort: [ "dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam." ],
        dayNamesMin: [ "D","L","M","M","J","V","S" ],
        weekHeader: "Sem.",
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "",
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        gotoCurrent: true,
        beforeShowDay: function(d) {
            var day = d.getDay();
            return [(day === 6 || day === 0)];
        },
        onSelect: function (dateText) {
            onChangeDate($dateTextHolder, dateText)
        }
    })

    onChangeWeekendType($typeWeekend, inputExternalName, inputSpeaker, inputOverseerName, inputSpeech, $talkReader, inputReader, inputChairman);
    setDate(inputDatepicker, $dateTextHolder, saturdayDate);
    checkAlreadyUse();
    setSelect2Input(inputSpeech, inputSpeaker, inputReader, inputChairman);
    onSelectSpeech(inputSpeech, inputDatepicker);
    addLinkModalOutgoingSpeaker($collectionHolder, index);

}



function setDate(inputDatepicker, $dateTextHolder, date) {
    inputDatepicker.datepicker('setDate', date)
}

function onChangeDate($dateTextHolder, dateStr) {
 //   $dateTextHolder.text(dateStr) TODO : Check why I putted this here
}

function onSelectSpeech($speech, $datepicker) {
    $speech.on('select2:select', function (e) {
        let data = e.params.data;
        $.ajax({
            url: '/rest/speech/last-time/' + data.id,
            success: function (data) {
                const datePickerDate = $datepicker.datepicker('getDate');
                const pickedDate = datePickerDate.getDate();
                const lastTimeDate = new Date(data.date);
                const diffTime = Math.abs(pickedDate - lastTimeDate.getDate());
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                const diffMonths = Math.floor(diffDays /30);
                let $infoLastTime = $($speech).closest('.speech-th').find('.info-last-time');

                if (diffMonths <= 3) { // TODO: definir une constante pour durée expiration discours
                    $infoLastTime.html('<div class="alert alert-info mt-2">Ce discours a été plannifié pour le ' + data.dateFr + '</div>')
                } else {
                    $infoLastTime.html('')
                }
            }
        })
    })
}


 /**
  * Generate select2 input
  * @param inputSpeech
  * @param $speaker
  * @param $reader
  * @param $chairman
  */
 function setSelect2Input(inputSpeech, $speaker, $reader, $chairman) {

    $($speaker).select2();
    $($reader).select2();
    $($chairman).select2();

    $(inputSpeech).select2({
        ajax: {
            url: '/rest/speech/autocomplete',
            data: function (params) {
                let query = {
                    key: params.term
                }
                return query;
            },
            processResults: function (data) {
                let response = [];
                for (let i = 0; i < data.length; i++ ) {
                    let item = {};
                    item.id = data[i].value
                    item.text = data[i].label
                    response.push(item)
                }
                return {
                    results : response
                }
            }
        }
    });
}


/**
 * Check if a Person is already use
 */
function checkAlreadyUse() {

    $(document).ready(function () {

        let $chairman = $('.input-chairman')
        let $reader = $('.input-reader')
        let $speaker = $('.input-speaker')

        $chairman.on('select2:select', function(e) {

            let parent = $(this).parent().parent();
            let arrReaders = parent.find('.input-reader').find('option');
            let arrSpeakers = parent.find('.input-speaker').find('option')
            let chairmanVal = $("option:selected", this).text()

            mapAndSetDisabled(arrReaders, chairmanVal)
            mapAndSetDisabled(arrSpeakers, chairmanVal)

        })


        $reader.on('select2:select', function(e) {

            let parent = $(this).parent().parent()
            let arrChairmen = parent.find('.input-chairman').find('option')
            let arrSpeakers = parent.find('.input-speaker').find('option')
            let readerVal = $("option:selected", this).text()

            mapAndSetDisabled(arrChairmen, readerVal)
            mapAndSetDisabled(arrSpeakers, readerVal)

        })


        $speaker.change(function(e) {

            let parent = $(this).parent().parent()
            let arrChairmen = parent.find('.input-chairman').find('option')
            let arrReaders = parent.find('.input-reader').find('option');
            let speakerVal = $("option:selected", this).text()

            mapAndSetDisabled(arrChairmen, speakerVal)
            mapAndSetDisabled(arrReaders, speakerVal)

        })


        function mapAndSetDisabled(array, valueToCompare) {
            array.map(function () {
                if (this.text === valueToCompare) {
                    $(this).attr('disabled', true)
                } else {
                    $(this).attr('disabled', false)
                }
            })
        }


    })
}

/**
 * Submit the form
 * @param form
 */
function submitForm(form)  {
    $(document).ready(function () {
        let id = $('.btn-validate').data('id');

        $('.btn-validate').click(function (e) {
            if (id) {
                form.attr('action', '/dashboard/programs/edit/' + id + '?month=' + month + '&year=' + year).submit()
            } else {
                form.attr('action', '/dashboard/programs/save?month=' + month + '&year=' + year).submit()
            }
        })

        $('.submit-form-program-draft').click(function (e) {
            if (id) {
                form.attr('action', '/dashboard/programs/edit/' + id + '?month=' + month + '&year=' + year + '&draft=true').submit()
            } else {
                form.attr('action', '/dashboard/programs/save?year=' + year + '&month=' + month + '&draft=true' ).submit()
            }


        })
    })
}

function handleType(type, externalName, inputSpeaker, overseerName, inputSpeech, reader, inputReader, inputChairman) {

    setDefaults(externalName, overseerName, inputSpeaker, inputReader, inputSpeech, inputChairman)

    if (type === '2') {
        checkExternalSpeaker(externalName, inputSpeaker)
    }
    if (type === '3') {
        checkOverseerSpeaker(overseerName, inputSpeaker, inputSpeech, reader, inputReader)
    }
    if (type !== '0' && type !== '3') {
        checkIsConvention(inputSpeaker, inputSpeech, inputChairman, inputReader)
    }
    if (type !== '0' && type !== '1' && type !== '2' && type !== '3') {
        checkIsConvention(inputSpeaker, inputSpeech, inputChairman, inputReader, true)
    }
}

function onChangeWeekendType(weekendType, externalName, inputSpeaker, overseerName, inputSpeech, reader, inputReader, inputChairman) {
    $(document).ready(function () {
        $(weekendType).change(function (e) {
            handleType(this.value, externalName, inputSpeaker, overseerName, inputSpeech, reader, inputReader, inputChairman)
        })
    })
}

function setDefaults(externalName, overseerName, inputSpeaker, inputReader, inputSpeech, inputChairman) {

    if (!inputSpeech.data('select2')) {
        inputSpeech.select2();
    }
    if (!inputSpeaker.data('select2')) {
        inputSpeaker.select2();
    }

    externalName.hasClass('hidden') === false ? externalName.addClass('hidden') : null;
    overseerName.addClass('hidden');
    inputSpeaker.removeClass('d-none')
    inputSpeaker.attr('disabled', false);
    inputReader.attr('disabled', false);
    inputSpeech.attr('disabled', false);
    inputChairman.attr('disabled', false);
    inputReader.hasClass('d-none') ? inputReader.removeClass('d-none') : null;

}

/**
 * Check if the speech will be given by an external speaker
 * @param externalName
 * @param inputSpeaker
 */
function checkExternalSpeaker(externalName, inputSpeaker) {

    $(inputSpeaker).select2('destroy');
    $(externalName).removeClass('hidden');
    $(inputSpeaker).addClass('d-none');
}

/**
 * Check if it's the overseer week
 * @param overseerName
 * @param speaker
 * @param speech
 * @param talkReader
 * @param inputReader
 */
function checkOverseerSpeaker(overseerName, speaker, speech, talkReader, inputReader)
{
    $(overseerName).removeClass('hidden');
    $(inputReader).attr('disabled', true)
    $(speaker).addClass('hidden')
    $(speech).attr('disabled', true);
    $(speaker).select2('destroy');
}

 /**
  *
  * @param speaker
  * @param speech
  * @param chairman
  * @param reader
  * @param isConvention
  */
 function checkIsConvention(speaker, speech, chairman, reader, isConvention = false) {

    $(speaker).attr('disabled', true);
    $(reader).attr('disabled', true);
    $(speech).attr('disabled', true);
    $(chairman).attr('disabled', !isConvention);
}



function loadEditView() {
    let itemSections = $('form').find('.items_section');
    let overseerName;
    let inputSpeaker;
    let inputSpeech;
    let $talkReader;
    let inputExternalName;
    let inputChairman;
    let inputReader;
    let $inputDatePicker;

    itemSections.map(function () {

        inputExternalName = $(this).find('.external-name');
        overseerName = $(this).find('.overseer-name');
        inputSpeaker = $(this).find('.input-speaker');
        inputSpeech = $(this).find('.input-speech');
        $talkReader = $(this).find('.talk_reader');
        inputChairman = $(this).find('.input-chairman');
        inputReader = $(this).find('.input-reader');
        $inputDatePicker = $(this).find('.datepicker');
        let $typeWeekend = $(this).find('.choice-type');

        setSelect2Input(inputSpeech, inputSpeaker, inputReader, inputChairman)
        handleType($typeWeekend.val(), inputExternalName, inputSpeaker, overseerName, inputSpeech, $talkReader, inputReader, inputChairman);
        onChangeWeekendType($typeWeekend, inputExternalName, inputSpeaker, overseerName, inputSpeech, $talkReader, inputReader, inputChairman);

    })
}

function addLinkModalOutgoingSpeaker($collectionHolder, index)
{
    let modal = $collectionHolder.data('modal');
    modal = modal.replace(/__name__/g, index);
    let $modalSection = $('.modals').append(modal);
    $collectionHolder.append($modalSection);
    let $launchModalSpan = $('.launch-modal-span' + index);
    $launchModalSpan.append('<a href="#" data-target="#modalOutgoing' + index + '" data-toggle="modal" class="launchModal">Orateur(s) sortant</a>')
}
