$(document).ready(function () {

    let $select = $('#selectPrograms');
    let initialID = $select.val();

    //on init, display a default program
    fetchProgramByID(initialID);

    $select.change(function () {
        fetchProgramByID(this.value);
    })

    function fetchProgramByID(id) {
        $.ajax({
            url: '/dashboard/programs/show/' + id,
            success: function (data) {
                $('#programContents').html(data)
            }
        })
    }

});