$(document).ready(function () {

    savePerson()

    function savePerson() {
        $('#formPerson').submit(function (e) {

            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '/dashboard/persons/save',
                data: $(this).serialize(),
                success: function (data) {
                    document.location.reload();
                },
                error: function (data) {
                    let res = data.responseJSON;
                        res.forEach(function (element) {
                            setAsInvalid(element.property, element.message)
                        })
                }
            })
        })
    }


    function setAsInvalid(property, message) {
        let $holder = $('.' + property);
        let $messageArea = $holder.closest('.col-md-6').find('.invalid-feedback');
        $holder.addClass('is-invalid');
        $messageArea.text(message)
    }
});